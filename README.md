# Housing Prices

Machine Learning project made for the exam "Statistical Methods For Machine Learning"

The goal of the project is to implement from scratch Ridge Regression algorithm to analyze a dataset of houses prices and predict them.
