import numpy
import pandas
import seaborn
import matplotlib.pyplot as plt
import pandas as pd
from typing import List

class Plots:

    # ------ Outliers ------

    @staticmethod
    def plotOutliers(data_set, columns):
        """
        Plot outliers
        Parameters
        ----------
        data_set Data set
        columns Columns to plot

        Returns
        -------

        """
        for name in columns:
            green_diamond = dict(markerfacecolor='g', marker='D')
            plt.figure(figsize=(15, 7))
            plt.title(name)
            if name == "Outliers":
                plt.boxplot(data_set.iloc[:, :-1], flierprops=green_diamond)
            else:
                plt.boxplot(data_set[name], flierprops=green_diamond)
            plt.show()

    # ------ Validation and Learning curves ------

    @staticmethod
    def getTrainTestScores(y):
        """
        Extract training and test scores

        :return: training and test scores
        """

        # Get train set scores
        train_set_score = y[0]
        # Get test set scores
        test_set_score = y[1]

        # Return results
        return train_set_score, test_set_score

    @staticmethod
    def getNegativePositiveScores(negative: bool):
        """
        Check if we need to make result negative or positive

        :return: negative or positive value
        """

        # Negative/positive value to return
        neg = 0

        # Check if MSE is negative
        if negative:
            neg = -1  # Make it positive
        else:
            neg = 1  # Leave it negative

        # Return result
        return neg

    @staticmethod
    def makeTrainTestScoreMeanStd(neg, train_set_score, test_set_score):
        """
        Calculate train/set scores mean/standard deviation

        :param train_set_score: training scores
        :param test_set_score: test scores
        :return: train and test scores mean and standard deviation
        """

        # Calculate mean train set scores
        train_set_scores_mean = neg * numpy.mean(train_set_score, axis=1)
        # Calculate standard deviation train set scores
        train_set_scores_std = numpy.std(train_set_score, axis=1)

        # Calculate mean test set scores
        test_set_scores_mean = neg * numpy.mean(test_set_score, axis=1)
        # Calculate standard deviation test set scores
        test_set_scores_std = numpy.std(test_set_score, axis=1)

        return train_set_scores_mean, train_set_scores_std, test_set_scores_mean, test_set_scores_std

    @staticmethod
    def plotValidationLearningCurve(x, y, labels: List[str], colors: List[str], negative: bool, xlabel: str, ylabel: str, title: str) -> None:
        """
        Plot validation or learning curve

        :param x: Alpha values
        :param y: Training and test scores
        :param labels: Labels of the curves
        :param colors: Colors of the curves
        :param negative: Negative (negative = neg_mean_square_error & co. / positive = r2 & co.)
        :param xlabel: x plot label
        :param ylabel: y plot label
        :param title: Plot title
        :return:

        References:
            - https://www.scikit-yb.org/en/develop/api/model_selection/validation_curve.html
            - https://www.scikit-yb.org/en/develop/api/model_selection/learning_curve.html
            - https://scikit-learn.org/stable/auto_examples/model_selection/plot_validation_curve.html#sphx-glr-auto-examples-model-selection-plot-validation-curve-py
            - https://jakevdp.github.io/PythonDataScienceHandbook/05.03-hyperparameters-and-model-validation.html
            - https://www.dataquest.io/blog/learning-curves-machine-learning/
        """

        # Check if loss function is negative
        neg = Plots.getNegativePositiveScores(negative)

        # Get training and test set scores
        train_set_score, test_set_score = Plots.getTrainTestScores(y)

        # Calculate mean and standard deviation of training and test set scores
        train_set_scores_mean, \
        train_set_scores_std, \
        test_set_scores_mean, \
        test_set_scores_std = Plots.makeTrainTestScoreMeanStd(neg, train_set_score, test_set_score)

        # Generate plot
        Plots.validationLearningPlot(
            x,                                                                                              # x
            [train_set_scores_mean, test_set_scores_mean],                                                  # y
            [train_set_scores_mean - train_set_scores_std, test_set_scores_mean - test_set_scores_std],     # x1 fill area
            [train_set_scores_mean + train_set_scores_std, test_set_scores_mean + test_set_scores_std],     # x2 fill area
            labels,                                                                                         # labels
            xlabel,                                                                                         # x label
            ylabel,                                                                                         # y label
            colors,                                                                                         # colors
            title)                                                                                          # title

    @staticmethod
    def validationLearningPlot(x, y, x1, x2, labels: List[str], x_label: str, y_label: str, colors: List[str], title: str) -> None:
        """
        Generates validation or learning plot

        Parameters
        ----------
        x: Alpha values
        y: Training and test scores
        x1: x1 area to fill with color
        x2: x2 area to fill with color
        labels: Labels of the curves
        x_label: x plot label
        y_label: y plot label
        colors: Colors of the curves
        title: Plot title

        Returns
        -------

        """
        # Generate plot
        for i in range(len(colors)):
            # Generate plot
            plt.plot(x, y[i], color=colors[i], label=labels[i])
            # Fill the area between the two vertical curves
            plt.fill_between(x, x1[i], x2[i], alpha=0.1, color=colors[i])

        # Set x label of the plot
        plt.xlabel(x_label)
        # Set y label of the plot
        plt.ylabel(y_label)
        # Set title of the plot
        plt.title(title)

        plt.grid()
        plt.legend()
        plt.show()

    # ------ Labels predicted ------

    @staticmethod
    def plotPredictions(y_real, y_predicted) -> None:
        """
        Plot y predictions using cross-validation and real y values

        :param y_predicted: y predicted
        :return:

        References:
            - https://scikit-learn.org/stable/auto_examples/model_selection/plot_cv_predict.html#sphx-glr-auto-examples-model-selection-plot-cv-predict-py.
        """

        fig, ax = plt.subplots()
        ax.scatter(y_real, y_predicted, c='#88c999', alpha=0.5, edgecolors=(0, 0, 0))
        ax.plot([y_real.min(), y_real.max()], [y_real.min(), y_real.max()], 'k--', lw=4)
        ax.set_xlabel('Measured')
        ax.set_ylabel('Predicted')
        plt.title("Label predicted with Cross-Validation")

        plt.grid()
        plt.show()

    # ------ Tuning alpha hyper-parameter ------

    @staticmethod
    def plotCoefficients(x_points, y_points, scale: str, x_label: str, y_label: str, title: str, reverse=False) -> None:
        """
        Plot coefficients and coefficient error as a function of alpha values

        :param x_points: x points of the plot
        :param y_points: y points of the plot
        :param scale: logarithmic (possible values: "linear", "log", "symlog", "logit")
        :param x_label: label of the x axis
        :param y_label: lavel of the y axis
        :param title: plot title
        :param reverse: reverse plot visualization
        :return:

        References:
            - https://scikit-learn.org/stable/auto_examples/linear_model/plot_ridge_coeffs.html.
            - https://scikit-learn.org/stable/auto_examples/linear_model/plot_ridge_path.html?highlight=plot%20ridge%20coefficients.
            - https://bradleyboehmke.github.io/HOML/regularized-regression.html#fig:ridge-coef-example
        """
        plt.subplot()
        ax = plt.gca()
        ax.plot(x_points, y_points)
        ax.set_xscale(scale)
        if reverse:
            ax.set_xlim(ax.get_xlim()[::-1])  # reverse axis
        plt.xlabel(x_label)
        plt.ylabel(y_label)
        plt.axis('tight')
        plt.title(title)

        plt.grid()
        plt.show()

    # ------ CV & NCV ------

    @staticmethod
    def plotNestedNonNestedCVScores(nested_scores, non_nested_scores) -> None:
        """
        Plot scores on each trial for nested and non-nested CV

        :return:

        References:
            - https://scikit-learn.org/stable/auto_examples/model_selection/plot_nested_cross_validation_iris.html
            - https://weina.me/nested-cross-validation/
            - https://chrisalbon.com/machine_learning/model_evaluation/nested_cross_validation/
            - https://towardsdatascience.com/nested-cross-validation-hyperparameter-optimization-and-model-selection-5885d84acda
            - https://machinelearningmastery.com/nested-cross-validation-for-machine-learning-with-python/
        """
        plt.figure()
        plt.subplot()
        non_nested_scores_line, = plt.plot(non_nested_scores, color='r')
        nested_line, = plt.plot(nested_scores, color='b')
        plt.ylabel("score r2", fontsize="14")
        plt.legend([non_nested_scores_line, nested_line],
                   ["Non-Nested CV", "Nested CV"],
                   bbox_to_anchor=(0, 1, .8, 0))
        plt.title("Non-Nested and Nested Cross Validation",
                  x=.5, y=1.2, fontsize="15")

        plt.grid()
        plt.show()

    @staticmethod
    def plotNestedNonNestedCVScoreDifference(NUMBER_TRIALS, score_difference) -> None:
        """
        Plot bar chart of the difference between non-nested and nested cv scores

        :return:

        References:
            - https://scikit-learn.org/stable/auto_examples/model_selection/plot_nested_cross_validation_iris.html
        """
        plt.subplot()
        difference_plot = plt.bar(range(NUMBER_TRIALS), score_difference)
        plt.xlabel("Individual Trial #")
        plt.legend([difference_plot],
                   ["Non-Nested CV - Nested CV Score"],
                   bbox_to_anchor=(0, 1, .8, 0))
        plt.ylabel("score difference", fontsize="14")

        plt.title("Nested and Non-nested CV Score difference",
                  x=.5, y=1.1, fontsize="15")

        plt.grid()
        plt.show()

    @staticmethod
    def plotValueOfCoefficients(title, x_train, regressor) -> None:
        """
        Plot coefficients (highest and lowest coefficients)

        :param x_train: x training set
        :param regressor: ridge regressor
        :return:

        References:
            - https://www.analyticsvidhya.com/blog/2017/06/a-comprehensive-guide-for-linear-ridge-and-lasso-regression/
        """
        # Checking the magnitude of coefficients
        predictors = x_train.columns

        # Getting coefficients
        coef = pd.Series(regressor.coef_, predictors).sort_values()

        # Plot coeffiencents
        coef.plot(kind='bar', title='Value of Coefficients')
        plt.show()

    # ------ PCA ------

    @staticmethod
    def plotCumulativeVariance(pca) -> None:
        """
        Plot cumulative variance

        :return:

        References:
            - https://stackoverflow.com/questions/53802098/how-to-choose-the-number-of-components-pca-scikitliear
            - https://www.mikulskibartosz.name/pca-how-to-choose-the-number-of-components/
        """

        plt.plot(numpy.cumsum(pca.explained_variance_ratio_))
        plt.xlabel('number of components')
        plt.ylabel('cumulative explained variance')
        plt.title('Cumulative variance increasing the number of components ')

        plt.grid()
        plt.show()

    @staticmethod
    def plotCutOffThreshold(pca) -> None:
        """
        Plot cumulative variance and a percentage of where we can "cut"
        :return:
        """

        #% matplotlib inline       # NOTE: delete this comment and use this line if you implement this code on Jupyter
        import matplotlib.pyplot as plt

        fig, ax = plt.subplots()
        xi = numpy.arange(1, 14, step=1)
        y = numpy.cumsum(pca.explained_variance_ratio_)

        plt.ylim(0.0, 1.1)
        plt.plot(xi, y, marker='o', linestyle='--', color='b')

        plt.xlabel('Number of Components')
        plt.xticks(numpy.arange(1, 14, step=1))  # change from 0-based array index to 1-based human-readable label
        plt.ylabel('Cumulative variance (%)')
        plt.title('The number of components needed to explain variance')

        plt.axhline(y=0.99, color='r', linestyle='-')
        plt.text(0.5, 0.94, '99% cut-off threshold', color='red', fontsize=16)

        ax.grid(axis='x')
        plt.show()