"""
=======================================================================================================
                                    Developing scikit-learn estimators
=======================================================================================================

This is a module to be used as a reference for building other modules

https://scikit-learn.org/stable/developers/develop.html?highlight=template#apis-of-scikit-learn-objects
https://github.com/scikit-learn-contrib/project-template/blob/master/skltemplate/_template.py
"""
import numpy as np

from sklearn.base import BaseEstimator
from sklearn.utils.validation import check_X_y, check_array, check_is_fitted
from sklearn.preprocessing import normalize
from sklearn.metrics import r2_score

"""
References:
- http://seismo.berkeley.edu/~kirchner/eps_120/Toolkits/Toolkit_10.pdf
- https://stackoverflow.com/questions/40557569/understanding-ridge-linear-regression-in-sci-kit-learn/40557888#40557888
- https://stats.stackexchange.com/questions/69205/how-to-derive-the-ridge-regression-solution (how to derive of ridge regression solution)
- https://stackoverflow.com/questions/50747922/python-sklearn-ridge-regression-normalize (important - explains how scikit ridge works)

We want to minimize this quantity:
    ||X*Theta - y||^2 + alpha * ||Theta||^2

where:
    - Theta = W <-------- coefficients/weights
    - ||Theta||^2 = sum of square of the magnitude of coefficients

and

    ||X*Theta - y||^2       +       alpha * ||Theta||^2
    |_______________|               |_________________|
            RSS             +   sum of square of coefficients

The closed form to calculate theta is:
    Theta = (X'X + G'G)^-1 X'y          <--- this is what we use

where:
    - G = alpha * I

"""


def preprocess_data(X, y):
    """
    We center the data such that a y-intercept parameter is not needed
    Parameters
    ----------
    X : data points
    y : labels

    Returns
    -------
    X : data points - weighted average
    y : y - weighted average
    X_average : weighted average of X
    y_average : weighted average of y

    """

    # What is reshape?: https://stackoverflow.com/questions/18691084/what-does-1-mean-in-numpy-reshape
    # Convert y as column vector
    y = y.reshape(-1, 1)

    # We center the data (such that a y-intercept parameter is not needed)

    # Weighted average along the X axis
    X_average = np.average(X, axis=0)
    # Update X subtracting weighted average
    X = X - X_average

    # Weighted average along the y axis
    y_average = np.average(y, axis=0)
    # Update y subtracting weighted average
    y = y - y_average

    return X, y, X_average, y_average


class RidgeRegressionEstimator(BaseEstimator):
    """ A template estimator to be used as a reference implementation.

    For more information regarding how to build your own estimator, read more
    in the :ref:`User Guide <user_guide>`.

    Parameters
    ----------
    alpha : float
        Alpha determines the regularization strength. The larger value for alpha,
        the stronger the regularization. In other words, when alpha is a very
        larger number, the bias of the model will be high. An alpha of 1, will
        result in a model that acts identical to Linear Regression.

    Examples
    --------
    from skltemplate import TemplateEstimator
    import numpy as np
    X = np.arange(100).reshape(100, 1)
    y = np.zeros((100, ))
    estimator = TemplateEstimator()
    estimator.fit(X, y)
    TemplateEstimator(demo_param='demo_param')
    """

    def __init__(self, alpha: float = 1.0):
        # Alpha hyperparameter
        self.alpha = alpha

    def fit(self, X, y):
        """
        Fit Ridge regression model.

        As first step is important to center and scale data (preprocessing).

        To do it we need to calculate X_average, y_average and X_scale where:
        - X_average = is the weighted average along the X axis.
        - y_average = is the weighted average along the y axis.
        - X_scale = is the L2 norm of X - X_offset

        X is given by X = (X - X_average) / X_scale.

        So, to normalize data, we subtract the mean (X_average) and divide by the l2-norm (X_scale)

        Parameters
        ----------
        X : {array-like, sparse matrix}, shape (n_samples, n_features)
            The training input samples.
        y : array-like, shape (n_samples,) or (n_samples, n_outputs)
            The target values (class labels in classification, real numbers in
            regression).
        Returns
        -------
        self : object
            Returns self.

        References:
            - https://stackoverflow.com/questions/50747922/python-sklearn-ridge-regression-normalize
            - http://faculty.cas.usf.edu/mbrannick/regression/regbas.html
            - http://seismo.berkeley.edu/~kirchner/eps_120/Toolkits/Toolkit_10.pdf
        """
        X, y = check_X_y(X, y, accept_sparse=True)
        self.is_fitted_ = True

        # Preprocess data
        X, y, X_average, y_average = preprocess_data(X, y)

        # Scale X data input vectors individually to unit norm (vector length), using L2 norm
        # NOTE: X_norms are calculated as np.sqrt((X * X).sum(axis=1)), that is using Euclidean norm of X
        X, X_norms = normalize(X=X, norm="l2", axis=0, copy=False, return_norm=True)

        # Calculate coefficients
        coefficients = self.calculate_coefficients(X, y)

        # Calculate coefficients (weights) diving by the L2 norm calculated
        self.coef_ = coefficients / X_norms

        # Calculate intercept
        self.intercept = y_average - np.dot(X_average, self.coef_.T)  # w₀ = y - Xwⁱ

        # `fit` should always return `self`
        return self

    # ================== PREDICT ==================

    def predict(self, X):
        """ A reference implementation of a predicting function.
        Parameters
        ----------
        X : {array-like, sparse matrix}, shape (n_samples, n_features)
            The training input samples.
        Returns
        -------
        y : ndarray, shape (n_samples,)
            Returns an array of ones.
        """
        X = check_array(X, accept_sparse=True)
        check_is_fitted(self, 'is_fitted_')

        # Calculate predicted values
        y_predicted = self.intercept + X @ self.coef_.T  # ŷ = w₀ + wXⁱ

        return y_predicted

    # ==================== SCORE ===================

    def score(self, x_test, y_test):
        """
        Calculate score using R-Squared loss function
        Parameters
        ----------
        x_test
        y_test

        Returns
        -------

        """
        # Calculate y predictions
        y_predicted = self.predict(x_test)

        # CalculateR-Squared  score
        score = r2_score(y_test, y_predicted)

        return score

    # ================= WEIGHTS =================

    def calculate_coefficients(self, X, y) -> np.ndarray:
        """
        Calculate coefficients w (weights)

        Parameters
        ----------
        X: Data points
        y: Labels

        Returns
        -------
        np.ndarray: Array of coefficients

        """
        # Calculate alpha * I
        G = self.alpha * np.identity(X.shape[1], dtype=np.float32)

        # Calculate X'X
        l2_X = np.dot(X.T, X)
        # Calculate G'G
        l2_G = np.dot(G.T, G)
        # Calculate X'y
        l2_Xy = np.dot(X.T, y)

        # Calculate coefficients
        coefficients = np.dot(np.linalg.inv(l2_X + l2_G), l2_Xy)  # normalized coefficients
        # Convert coefficients array as column vector
        coefficients = coefficients.reshape(-1)

        return coefficients